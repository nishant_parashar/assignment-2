var submit = document.getElementById('submit');
var fname = document.getElementById('firstName');
var lname = document.getElementById('lastName');
var phone = document.getElementById('phone');
var office = document.getElementById('office');
var email = document.getElementById('email');
var password = document.getElementById('password');
var conPass = document.getElementById('conPassword');
var age = document.getElementById('age');
var aboutYou = document.getElementById('aboutYou');
var radios = document.getElementsByName('radio');

var year = document.getElementById('year');
var month = document.getElementById('month');
var day = document.getElementById('day');

age.disabled = true;


submit.addEventListener('click', (e) => {
    e.preventDefault();
    validateForm ();
    checkIsChecked();
    validateEmail();
});

function checkIsChecked(){
    var inputs = document.getElementsByClassName('checkbox');
    var flag = false;
    for(var i = 0; i < inputs.length; i++) {
        if(inputs[i].checked) {
            flag = true;
        }
    }
    if(!flag){
        alert("Please Check At Least One Interest");
        return false;
    }
}

function calcDate() {
            
    var year = document.getElementById('year').value;
    var month = document.getElementById('month').value;
    var day = document.getElementById('day').value;
    
today = new Date()
past = new Date(year,month,day)
var diff = Math.floor(today.getTime() - past.getTime());

age.value= (diff/(1000 * 60 * 60 * 24*365)).toFixed(2);
}

document.getElementById('year').addEventListener("change", calcDate);

function validateEmail() {
    var emailText = document.getElementById('email').value;
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;

    if (pattern.test(emailText)) {
        return true;
    } else {
        alert('Invalid Email Address');
        return false;
    }
}

function validateForm() {
    var fnameVal = fname.value;
    var lnameVal = lname.value;
    var emailVal = email.value;
    var passVal = password.value;
    var conpassVal = conPass.value;
    var phoneVal = phone.value;
    var officeVal = office.value;
    var aboutVal = aboutYou.value;
    
    var dayVal = day.options[day.selectedIndex].value;
    var monthVal = month.options[month.selectedIndex].value;
    var yearVal = year.options[year.selectedIndex].value;
    
    var formValid = false;
    var i=0;
    


    if(fnameVal==='') {
        alert("Please Enter Your First Name.");
    }

    if(lnameVal==='') {
        alert("Please Enter Your Last Name");
    }

    if(phoneVal==='') {
        alert("Please Enter Your Phone Number");
    }

    if(phoneVal.length != 10) {
        alert("Phone Number Should be 10 Digits");
    }

    if(isNaN(phoneVal)) {
        alert("Phone Number Should Be A Numeric Value");
    }

    if(isNaN(officeVal)) {
        alert("Office Number Should Be A Numberic Value");
    }

    if(emailVal==='') {
        alert("Please Enter Your Email ID");
    }

    if(passVal==='') {
        alert("Please Enter Your Password");
    }

    if(/[^a-zA-Z0-9]/.test(passVal)) {
        alert('Password Should Not Contain Special Characters');
    }

    if((passVal.length > 12) || (passVal.length < 8)) {
        alert("Password Should Be 8-12 Characters Long");
    }

    if(conpassVal==='') {
        alert("Confirm Your Password");
    }

    if(passVal != conpassVal) {
        alert("Passwords Don't Match");
    }

    while(!formValid && i < radios.length) {
        if(radios[i].checked) {
            formValid = true;
        }
        i++;
    }

    if(!formValid) {
        alert("Please Select Gender");
    }

    if(aboutVal===''){
        alert("Please Write Something About You")
    }

    if(dayVal==0) {
        alert("Select Day");
    }
    if(monthVal==0) {
        alert("Select Month");
    }
    if(yearVal==0) {
        alert("Select Year");
    }
}
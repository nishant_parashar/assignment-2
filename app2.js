var submit = document.getElementById('submit');
var fname = document.getElementById('firstName');
var lname = document.getElementById('lastName');
var phone = document.getElementById('phone');
var office = document.getElementById('office');
var pass = document.getElementById('password');

var fnerror = document.getElementById('fnerror');
var lnerror = document.getElementById('lnerror');
var pherror = document.getElementById('pherror');
var offerror = document.getElementById('offerror');
var passerror = document.getElementById('passerror');

submit.addEventListener('click', (e) => {
    e.preventDefault();
    checkIsChecked();
});

var formValid = false;
var i=0;

age.disabled = true;

function fnameCheck() {
    var regex = /^[A-Za-z]+$/;
    if(!fname.value.match(regex)) {
        fname.style.border="1px solid red";
        fnerror.style.color="red";
        fnerror.innerHTML="*Please use only text";
    }else {
        fname.style.border="1px solid #e3e3e3";
        fnerror.innerHTML="";
    }
}

function fnValidate() {
    var letters = /^[A-Za-z]+$/;
    if(fname.value == "") {
        alert("Please enter your first name");
    }else if(!fname.value.match(letters)) {
        alert("Please enter only letters");
    }
}

function lnameCheck() {
    var regex = /^[A-Za-z]+$/;
    if(!lname.value.match(regex)) {
        lname.style.border="1px solid red";
        lnerror.style.color="red";
        lnerror.innerHTML="*Please use only text";
    }else {
        lname.style.border="1px solid #e3e3e3";
        lnerror.innerHTML="";
    }
}

function lnValidate() {
    var letters = /^[A-Za-z]+$/;
    if(lname.value == "") {
        alert("Please enter your last name");
    }else if(!lname.value.match(letters)) {
        alert("Please enter only letters");
    }
}

function phoneCheck() {
    var regex = /^[0-9]+$/;
    if(!phone.value.match(regex)) {
        phone.style.border="1px solid red";
        pherror.style.color="red";
        pherror.innerHTML="*Please use only numbers";
    }else if(phone.value.length != 10) {
        phone.style.border="1px solid red";
        pherror.style.color="red";
        pherror.innerHTML="*Phone Number should not exceed 10 digits";
    } else {
        phone.style.border="1px solid #e3e3e3";
        pherror.innerHTML="";
    }
}

function phoneValidate() {
    if(phone.value == '') {
        alert('Please Enter your Phone Number');
    }else if(isNaN(phone.value)) {
        alert("Input only Numbers");
    }
}

function officeCheck() {
    var regex = /^[0-9]+$/;
    if(office.value.match(regex)) {
        office.style.border="1px solid #e3e3e3";
        fnerror.innerHTML="";
    }else {
        office.style.border="1px solid red";
        offerror.style.color="red";
        offerror.innerHTML="*Please use only numbers";
    }
}

function officeValidate() {
    if(isNaN(office.value)) {
        alert("Input only Numbers");
    }
}

function emailValidate() {
    var emailText = document.getElementById('email').value;
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;

    if (pattern.test(emailText)) {
        return true;
    }else {
        alert('Invalid Email Address');
        return false;
    }
}

function passCheck() {
    var regex = /^[a-zA-Z0-9]+$/;
    if(pass.value.match(regex)) {
        pass.style.border="1px solid #e3e3e3";
        passerror.innerHTML="";
    }else {
        pass.style.border="1px solid red";
        passerror.style.color="red";
        passerror.innerHTML="*Please use only alphanumeric values";
    }
}

function passValidate() {
    if(pass.value == '') {
        alert("Please enter a password");
    }else if(/[^a-zA-Z0-9]/.test(pass.value)) {
        alert("The password should not contain special characters");
    }else if(pass.value.length < 8 || pass.value.length > 12) {
        alert("The password should be between 8-12 characters");
    }
}

function conpassValidate() {
    var conpass = document.getElementById('conPassword');
    if(conpass.value == '') {
        alert("Please confirm your password");
    }else if(conpass.value !== pass.value) {
        alert("Passwords do not match")
    }
}

function aboutValidate() {
    var about = document.getElementById('aboutYou');
    if(about.value == '') {
        alert('Please tell us something about you');
    }
}

function genderValidate() {
    while(!formValid && i < radios.length) {
        if(radios[i].checked) {
            formValid = true;
        }
        i++;
    }

    if(formValid == false) {
        alert("Please Select Gender");
    }
}

function checkIsChecked(){
    var inputs = document.getElementsByClassName('checkbox');
    var flag = false;
    for(var i = 0; i < inputs.length; i++) {
        if(inputs[i].checked) {
            flag = true;
        }
    }
    if(!flag){
        alert("Please Check At Least One Interest");
        return false;
    }
}

function calcDate() {
            
    var year = document.getElementById('year').value;
    var month = document.getElementById('month').value;
    var day = document.getElementById('day').value;
    
today = new Date()
past = new Date(year,month,day)
var diff = Math.floor(today.getTime() - past.getTime());

age.value= (diff/(1000 * 60 * 60 * 24*365)).toFixed(2);

if(age.value<18) {
    alert("You need to be over the age of 18");
}
}

document.getElementById('year').addEventListener("change", calcDate);

function call() {
    var kcyear = document.getElementsByName("year")[0],
    kcmonth = document.getElementsByName("month")[0],
    kcday = document.getElementsByName("day")[0];
        
    var d = new Date();
    var n = d.getFullYear();
    for (var i = n; i >= 1950; i--) {
        var opt = new Option();
        opt.value = opt.text = i;
        kcyear.add(opt);
    }
    
    kcyear.addEventListener("change", validate_date);
    kcmonth.addEventListener("change", validate_date);

    function validate_date() {
        var y = +kcyear.value, m = kcmonth.value, d = kcday.value;
        if (m === "2")
            var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
        else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
        kcday.length = 0;
        for (var i = 1; i <= mlength; i++) {
            var opt = new Option();
            opt.value = opt.text = i;
            if (i == d) opt.selected = true;
            kcday.add(opt);
        }
    }
        validate_date();
}